#!/usr/bin/python3
import zhtts
import sys

if __name__ == '__main__':
    if len(sys.argv)==2:
        fn = sys.argv[1]
        print(fn)
    text = ''
    tts = zhtts.TTS()
    while True:
        try:
            s = input()
            text = text+'。'+s
        except:
            break
    tts.text2wav(text, fn)
        
